/**
 * 
 */
package com.upax.employee.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.upax.employee.entity.EmployeeWorkedHoursEntity;

/**
 * <strong>IEmployeeWorkedHoursRepository.java</strong><br>
 * Repository class for employee worked hours sentences.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

public interface IEmployeeWorkedHoursRepository extends CrudRepository<EmployeeWorkedHoursEntity, Long> {

    @Query(value = "select * from employee_worked_hours w where w.employee_id = :employee_id and CAST(w.worked_date as date) = CAST(:worked_date as date)", nativeQuery = true)
    public Optional<EmployeeWorkedHoursEntity> findByEmployeeAndWorkedDate(@Param("employee_id") Long employeeId,
            @Param("worked_date") Date workedDate);

    @Query(value = "select * from employee_worked_hours w where w.employee_id = :employee_id and CAST(worked_date AS date) between CAST(:start_date AS date) and CAST(:end_date AS date)", nativeQuery = true)
    public Optional<List<EmployeeWorkedHoursEntity>> findAllByEmployeeIdAndWorkedDates(
            @Param("employee_id") Long employeeId, @Param("start_date") Date startDate,
            @Param("end_date") Date endDate);
}

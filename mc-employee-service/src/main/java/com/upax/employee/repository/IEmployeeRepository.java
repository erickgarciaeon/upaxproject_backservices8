/**
 * 
 */
package com.upax.employee.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.upax.employee.entity.EmployeeEntity;
import com.upax.employee.entity.JobEntity;

/**
 * <strong>IEmployeeRepository.java</strong><br>
 * Repository class for employee sentences.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@Repository
public interface IEmployeeRepository extends CrudRepository<EmployeeEntity, Long> {

    @Query(value = "select * from employees e where lower(e.name) = lower(:name) and lower(e.last_name) = lower(:last_name)", nativeQuery = true)
    public Optional<EmployeeEntity> findByFullName(@Param("name") String name, @Param("last_name") String lastName);

    public List<EmployeeEntity> findByJob(JobEntity job);

}

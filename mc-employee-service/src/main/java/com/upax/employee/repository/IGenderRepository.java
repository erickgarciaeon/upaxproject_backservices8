/**
 * 
 */
package com.upax.employee.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.upax.employee.entity.GenderEntity;

/**
 * <strong>IGenderRepository.java</strong><br>
 * Repository class for {@link GenderEntity} Entity.<br>
 * <br>
 * Version control:<br>
 * <ol>
 *      <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez | golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@Repository
public interface IGenderRepository extends CrudRepository<GenderEntity, Long> {
}

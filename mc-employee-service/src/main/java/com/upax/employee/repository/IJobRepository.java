/**
 * 
 */
package com.upax.employee.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.upax.employee.entity.JobEntity;

/**
 * <strong>IJobRepository.java</strong><br>
 * TODO Add class description.<br>
 * <br>
 * Version control:<br>
 * <ol>
 *      <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez | golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@Repository
public interface IJobRepository extends CrudRepository<JobEntity, Long> {
}

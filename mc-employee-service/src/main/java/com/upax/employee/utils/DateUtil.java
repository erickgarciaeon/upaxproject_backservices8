/**
 * 
 */
package com.upax.employee.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * <strong>DateUtil.java</strong><br>
 * Util class for Dates.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

public final class DateUtil {

    /**
     * Constructor of DateUtil.
     */
    private DateUtil() {
    }

    /**
     * Calculate the number of years between two dates
     * 
     * @param startDate
     *            {@link Date} Start date.
     * @param endDate
     *            {@link Date} End date.
     * @return {@link Long} Number of years between two dates.
     */
    public static Long getYearsBetweenDates(final Date startDate, final Date endDate) {
        if (startDate == null || endDate == null) {
            return 0L;
        }
        LocalDateTime startDateTime = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime endDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        return ChronoUnit.YEARS.between(startDateTime, endDateTime);
    }

}

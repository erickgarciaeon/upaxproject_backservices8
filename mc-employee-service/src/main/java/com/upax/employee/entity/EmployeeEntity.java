/**
 * 
 */
package com.upax.employee.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * <strong>EmployeeEntity.java</strong><br>
 * Class Entity for EMPLOYEE Table.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Entity(name = "employees")
public class EmployeeEntity implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 4758213208554904958L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private GenderEntity gender;

    @OneToOne
    private JobEntity job;

    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birthdate")
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    /**
     * @return the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the gender.
     */
    public GenderEntity getGender() {
        return gender;
    }

    /**
     * @param gender
     *            the gender to set.
     */
    public void setGender(GenderEntity gender) {
        this.gender = gender;
    }

    /**
     * @return the job.
     */
    public JobEntity getJob() {
        return job;
    }

    /**
     * @param job
     *            the job to set.
     */
    public void setJob(JobEntity job) {
        this.job = job;
    }

    /**
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the lastName.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the birthDate.
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate
     *            the birthDate to set.
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

}

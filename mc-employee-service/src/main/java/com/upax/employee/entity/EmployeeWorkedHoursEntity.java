/**
 * 
 */
package com.upax.employee.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * <strong>EmployeeWorkedHoursEntity.java</strong><br>
 * Class Entity for EMPLOYEE_WORKED_HOURS Table.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@Entity(name = "employee_worked_hours")
public class EmployeeWorkedHoursEntity implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -2171456389498090802L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private EmployeeEntity employee;

    @Column(name = "worked_hours")
    private Integer workedHours;

    @Column(name = "worked_date")
    @Temporal(TemporalType.DATE)
    private Date workedDate;

    /**
     * @return the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the employee.
     */
    public EmployeeEntity getEmployee() {
        return employee;
    }

    /**
     * @param employee
     *            the employee to set.
     */
    public void setEmployee(EmployeeEntity employee) {
        this.employee = employee;
    }

    /**
     * @return the workedHours.
     */
    public Integer getWorkedHours() {
        return workedHours;
    }

    /**
     * @param workedHours
     *            the workedHours to set.
     */
    public void setWorkedHours(Integer workedHours) {
        this.workedHours = workedHours;
    }

    /**
     * @return the workedDate.
     */
    public Date getWorkedDate() {
        return workedDate;
    }

    /**
     * @param workedDate
     *            the workedDate to set.
     */
    public void setWorkedDate(Date workedDate) {
        this.workedDate = workedDate;
    }

}

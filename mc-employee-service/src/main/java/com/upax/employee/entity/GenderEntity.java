/**
 * 
 */
package com.upax.employee.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * <strong>GenderEntity.java</strong><br>
 * Class Entity for genders Table.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@Entity(name = "genders")
public class GenderEntity implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    /**
     * @return the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

}

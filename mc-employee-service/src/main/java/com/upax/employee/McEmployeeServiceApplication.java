package com.upax.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class McEmployeeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(McEmployeeServiceApplication.class, args);
	}

}

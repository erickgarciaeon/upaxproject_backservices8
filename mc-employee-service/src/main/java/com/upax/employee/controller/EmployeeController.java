/**
 * 
 */
package com.upax.employee.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.upax.employee.entity.EmployeeEntity;
import com.upax.employee.model.EmployeByIdModel;
import com.upax.employee.model.EmployeeModel;
import com.upax.employee.service.IEmployeeService;

/**
 * <strong>EmployeeController.java</strong><br>
 * Controller for Employee operations.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @GetMapping("/list")
    public ResponseEntity<List<EmployeeEntity>> findAll() {
        return new ResponseEntity<List<EmployeeEntity>>(employeeService.findAll(), HttpStatus.OK);
    }

    @GetMapping("list/byJob")
    @ResponseBody
    public ResponseEntity<Object> findByJob(@Valid @RequestBody EmployeByIdModel employeByIdModel) {
        List<EmployeeEntity> listEmployees = employeeService.findByJob(employeByIdModel);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("employees", listEmployees);
        response.put("success", (!listEmployees.isEmpty()));
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity<Object> save(@Valid @RequestBody EmployeeModel employee) {
        EmployeeEntity newEmployee = employeeService.save(employee);
        boolean success = (newEmployee != null);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("id", (success) ? newEmployee.getId() : null);
        response.put("success", success);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

}

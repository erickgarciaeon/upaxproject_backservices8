/**
 * 
 */
package com.upax.employee.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.upax.employee.entity.EmployeeWorkedHoursEntity;
import com.upax.employee.model.EmployeeWorkedHoursModel;
import com.upax.employee.model.TotalWorkedHoursModel;
import com.upax.employee.service.IEmployeeWorkedHoursService;

/**
 * <strong>WorkedHoursController.java</strong><br>
 * Controller for WorkedHoursEmployee operations.<br>
 * <br>
 * Version control:<br>
 * <ol>
 *      <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez | golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@RestController
@RequestMapping("workedhours")
public class WorkedHoursController {
    @Autowired
    private IEmployeeWorkedHoursService employeeWorkedHoursService;

    @GetMapping("/registerHours/list")
    public ResponseEntity<List<EmployeeWorkedHoursEntity>> findAllWorkedHours() {
        return new ResponseEntity<List<EmployeeWorkedHoursEntity>>(employeeWorkedHoursService.findAll(), HttpStatus.OK);
    }
    
    @PostMapping("/registerHours")
    @ResponseBody
    public ResponseEntity<Object> registerWorkedHours(@Valid @RequestBody EmployeeWorkedHoursModel workedHours) {
        EmployeeWorkedHoursEntity newWorkedHours = employeeWorkedHoursService.save(workedHours);
        boolean success = (newWorkedHours != null);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("id", (success) ? newWorkedHours.getId() : null);
        response.put("success", success);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }
    
    @GetMapping("/totalWorkedHours")
    public ResponseEntity<Object> getTotaWordedHours(@Valid @RequestBody TotalWorkedHoursModel workedHours) {
        List<EmployeeWorkedHoursEntity> listHours = employeeWorkedHoursService.getTotalWorkedHours(workedHours);

        boolean success = (!listHours.isEmpty());
        Double total = listHours.stream().mapToDouble(w -> w.getWorkedHours()).sum();

        Map<String, Object> json = new HashMap<>();
        json.put("total", (success) ? total : null);
        json.put("success", success);

        return new ResponseEntity<Object>(json, HttpStatus.OK);
    }

}

/**
 * 
 */
package com.upax.employee.service;

import java.util.List;

import com.upax.employee.entity.EmployeeEntity;
import com.upax.employee.model.EmployeByIdModel;
import com.upax.employee.model.EmployeeModel;

/**
 * <strong>IEmployeeService.java</strong><br>
 * Service class for {@link EmployeeEntity}.<br>
 * <br>
 * Version control:<br>
 * <ol>
 *      <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez | golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
public interface IEmployeeService {
    public List<EmployeeEntity> findAll();
    public List<EmployeeEntity> findByJob(EmployeByIdModel employeByIdModel);
    public EmployeeEntity save(EmployeeModel employee);
}

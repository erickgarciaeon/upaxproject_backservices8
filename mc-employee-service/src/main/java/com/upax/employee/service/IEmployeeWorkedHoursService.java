/**
 * 
 */
package com.upax.employee.service;

import java.util.List;
import com.upax.employee.entity.EmployeeWorkedHoursEntity;
import com.upax.employee.model.EmployeeWorkedHoursModel;
import com.upax.employee.model.TotalWorkedHoursModel;

/**
 * <strong>IEmployeeWorkedHoursService.java</strong><br>
 * Service class for {@link EmployeeWorkedHoursEntity}.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
public interface IEmployeeWorkedHoursService {
    public List<EmployeeWorkedHoursEntity> findAll();
    public EmployeeWorkedHoursEntity save(final EmployeeWorkedHoursModel workedHours);
    public List<EmployeeWorkedHoursEntity> getTotalWorkedHours(TotalWorkedHoursModel workedHours);
}

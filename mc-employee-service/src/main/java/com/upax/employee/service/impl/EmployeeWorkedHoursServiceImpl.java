/**
 * 
 */
package com.upax.employee.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upax.employee.entity.EmployeeEntity;
import com.upax.employee.entity.EmployeeWorkedHoursEntity;
import com.upax.employee.model.EmployeeWorkedHoursModel;
import com.upax.employee.model.TotalWorkedHoursModel;
import com.upax.employee.repository.IEmployeeRepository;
import com.upax.employee.repository.IEmployeeWorkedHoursRepository;
import com.upax.employee.service.IEmployeeWorkedHoursService;

/**
 * <strong>EmployeeWorkedHoursServiceImpl.java</strong><br>
 * Implementation class for interface {@link IEmployeeWorkedHoursService}.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@Service
public class EmployeeWorkedHoursServiceImpl implements IEmployeeWorkedHoursService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeWorkedHoursServiceImpl.class);

    @Autowired
    private IEmployeeWorkedHoursRepository workedHoursRepository;

    @Autowired
    private IEmployeeRepository employeeRepository;

    @Override
    public List<EmployeeWorkedHoursEntity> findAll() {
        return (List<EmployeeWorkedHoursEntity>) workedHoursRepository.findAll();
    }

    @Override
    public EmployeeWorkedHoursEntity save(EmployeeWorkedHoursModel workedHours) {
        // Validate hours.
        LOG.info("Validating hours...");
        if (workedHours.getWorkedHours() > 20) {
            LOG.info("Hours are greater than 20");
            return null;
        }

        // Validate worked date.
        LOG.info("Validating worked hours...");
        if (workedHours.getWorkedDate().after(new Date())) {
            LOG.info("The date is greater than today");
            return null;
        }

        // Validate if worked date exists.
        LOG.info("Validating worked hours exists...");
        Optional<EmployeeWorkedHoursEntity> workedHoursFound = workedHoursRepository
                .findByEmployeeAndWorkedDate(workedHours.getEmployeId(), workedHours.getWorkedDate());
        if (workedHoursFound.isPresent()) {
            LOG.info("Worked hours already exists");
            return null;
        }

        // Employee validation.
        Optional<EmployeeEntity> employeFound = employeeRepository.findById(workedHours.getEmployeId());
        LOG.info("Validating if employee exist...");
        if (employeFound.isEmpty()) {
            LOG.info("Employee not exists...");
            return null;
        }

        EmployeeWorkedHoursEntity newHours = new EmployeeWorkedHoursEntity();
        newHours.setEmployee(employeFound.get());
        newHours.setWorkedDate(workedHours.getWorkedDate());
        newHours.setWorkedHours(workedHours.getWorkedHours());

        return workedHoursRepository.save(newHours);
    }

    @Override
    public List<EmployeeWorkedHoursEntity> getTotalWorkedHours(TotalWorkedHoursModel workedHours) {
        LOG.info("Validate worked dates...");
        if (workedHours.getStartDate().after(workedHours.getEndDate())) {
            LOG.info("Start date is more than end Date...");
            return List.of();
        }

        LOG.info("Validate if employee exists...");
        Optional<EmployeeEntity> oEmployee = employeeRepository.findById(workedHours.getEmployeeId());
        if (oEmployee.isEmpty()) {
            LOG.info("Employee not exists...");
            return List.of();
        }

        Optional<List<EmployeeWorkedHoursEntity>> list = workedHoursRepository.findAllByEmployeeIdAndWorkedDates(
                workedHours.getEmployeeId(), workedHours.getStartDate(), workedHours.getEndDate());
        
        return list.get();
    }

    
    
}

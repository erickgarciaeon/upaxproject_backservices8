/**
 * 
 */
package com.upax.employee.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upax.employee.entity.EmployeeEntity;
import com.upax.employee.entity.GenderEntity;
import com.upax.employee.entity.JobEntity;
import com.upax.employee.model.EmployeByIdModel;
import com.upax.employee.model.EmployeeModel;
import com.upax.employee.repository.IEmployeeRepository;
import com.upax.employee.repository.IGenderRepository;
import com.upax.employee.repository.IJobRepository;
import com.upax.employee.service.IEmployeeService;
import com.upax.employee.utils.DateUtil;

/**
 * <strong>EmployeeServiceImpl.java</strong><br>
 * Implementation class for interface {@link IEmployeeService}.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@Service
public class EmployeeServiceImpl implements IEmployeeService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private IEmployeeRepository employeeRepository;
    @Autowired
    private IGenderRepository genderRepository;
    @Autowired
    private IJobRepository jobRepository;

    @Override
    public List<EmployeeEntity> findAll() {
        return (List<EmployeeEntity>) employeeRepository.findAll();
    }

    @Override
    public List<EmployeeEntity> findByJob(EmployeByIdModel employeByIdModel) {
        Optional<JobEntity> jobFound = jobRepository.findById(employeByIdModel.getJobId());
        LOG.info("Validating if job exist...");
        if (jobFound.isEmpty()) {
            LOG.info("Job does not exist...");
            return List.of();
        }
        return employeeRepository.findByJob(jobFound.get());
    }

    @Override
    public EmployeeEntity save(EmployeeModel employee) {
        // Name and lastName validation.
        Optional<EmployeeEntity> employeFound = employeeRepository.findByFullName(employee.getName(),
                employee.getLastName());
        LOG.info("Validating if employee exist...");
        if (employeFound.isPresent()) {
            LOG.info("Employee already exists...");
            return null;
        }

        // Gender validation.
        Optional<GenderEntity> genderFound = genderRepository.findById(employee.getGenderId());
        LOG.info("Validating if gender exist...");
        if (genderFound.isEmpty()) {
            LOG.info("Gender does not exist...");
            return null;
        }

        // Job validation.
        Optional<JobEntity> jobFound = jobRepository.findById(employee.getJobId());
        LOG.info("Validating if job exist...");
        if (jobFound.isEmpty()) {
            LOG.info("Job does not exist...");
            return null;
        }

        // Years validation.
        Long years = DateUtil.getYearsBetweenDates(employee.getBirthDate(), new Date());
        LOG.info("Validating years of employee...");
        LOG.info("Employee is [" + years + "] years old.");
        if (years < 18) {
            LOG.info("Employee is very young");
            return null;
        }

        EmployeeEntity newEmployee = new EmployeeEntity();
        newEmployee.setName(employee.getName());
        newEmployee.setLastName(employee.getLastName());
        newEmployee.setBirthDate(employee.getBirthDate());
        newEmployee.setGender(genderFound.get());
        newEmployee.setJob(jobFound.get());

        return employeeRepository.save(newEmployee);
    }

}

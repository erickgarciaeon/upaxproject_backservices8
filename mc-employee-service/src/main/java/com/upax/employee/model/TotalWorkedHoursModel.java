/**
 * 
 */
package com.upax.employee.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.upax.employee.entity.EmployeeWorkedHoursEntity;

/**
 * <strong>TotalWorkedHoursModel.java</strong><br>
 * Model class for {@link EmployeeWorkedHoursEntity} request.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

public class TotalWorkedHoursModel implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1678963565528678661L;

    @JsonProperty("employee_id")
    @NotNull
    private Long employeeId;

    @JsonProperty("start_date")
    @NotNull
    private Date startDate;

    @JsonProperty("end_date")
    @NotNull
    private Date endDate;

    /**
     * @return the employeeId.
     */
    public Long getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId
     *            the employeeId to set.
     */
    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the startDate.
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate.
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set.
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}

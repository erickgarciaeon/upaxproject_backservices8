/**
 * 
 */
package com.upax.employee.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.upax.employee.entity.EmployeeWorkedHoursEntity;

/**
 * <strong>EmployeeWorkedHoursModel.java</strong><br>
 * Model class for {@link EmployeeWorkedHoursEntity} request.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
public class EmployeeWorkedHoursModel implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -4408378466808301348L;

    @JsonProperty("employee_id")
    @NotNull
    private Long employeId;

    @JsonProperty("worked_hours")
    @NotNull
    private Integer workedHours;

    @JsonProperty("worked_date")
    @NotNull
    private Date workedDate;

    /**
     * @return the employeId.
     */
    public Long getEmployeId() {
        return employeId;
    }

    /**
     * @param employeId
     *            the employeId to set.
     */
    public void setEmployeId(Long employeId) {
        this.employeId = employeId;
    }

    /**
     * @return the workedHours.
     */
    public Integer getWorkedHours() {
        return workedHours;
    }

    /**
     * @param workedHours
     *            the workedHours to set.
     */
    public void setWorkedHours(Integer workedHours) {
        this.workedHours = workedHours;
    }

    /**
     * @return the workedDate.
     */
    public Date getWorkedDate() {
        return workedDate;
    }

    /**
     * @param workedDate
     *            the workedDate to set.
     */
    public void setWorkedDate(Date workedDate) {
        this.workedDate = workedDate;
    }

}

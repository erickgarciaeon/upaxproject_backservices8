/**
 * 
 */
package com.upax.employee.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.upax.employee.entity.EmployeeEntity;
import com.upax.employee.entity.EmployeeWorkedHoursEntity;

/**
 * <strong>EmployeByIdModel.java</strong><br>
 * Model class for {@link EmployeeEntity} request.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
public class EmployeByIdModel implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 2655949554917704968L;

    @JsonProperty("job_id")
    @NotNull
    private Long jobId;

    /**
     * @return the jobId.
     */
    public Long getJobId() {
        return jobId;
    }

    /**
     * @param jobId
     *            the jobId to set.
     */
    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

}

/**
 * 
 */
package com.upax.employee.model;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.upax.employee.entity.EmployeeEntity;

/**
 * <strong>EmployeeModel.java</strong><br>
 * Model class for {@link EmployeeEntity} request.<br>
 * <br>
 * Version control:<br>
 * <ol>
 * <li>1.0.0 | 08/07/2021 | Gustavo Olivares Hernandez |
 * golivaresh.dev@gmail.com</li>
 * </ol>
 * 
 * @author golivaresh
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
public class EmployeeModel implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -5516360872287632634L;

    @NotNull
    @JsonProperty("gender_id")
    private Long genderId;

    @NotNull
    @JsonProperty("job_id")
    private Long jobId;

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    @NotBlank
    @JsonProperty("last_name")
    private String lastName;

    @NotNull
    @JsonProperty("birthdate")
    private Date birthDate;

    /**
     * @return the genderId.
     */
    public Long getGenderId() {
        return genderId;
    }

    /**
     * @param genderId
     *            the genderId to set.
     */
    public void setGenderId(Long genderId) {
        this.genderId = genderId;
    }

    /**
     * @return the jobId.
     */
    public Long getJobId() {
        return jobId;
    }

    /**
     * @param jobId
     *            the jobId to set.
     */
    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    /**
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the lastName.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the birthDate.
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate
     *            the birthDate to set.
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

}
